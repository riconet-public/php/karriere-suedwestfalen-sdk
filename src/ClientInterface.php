<?php

/**
 * MIT License
 *
 * Copyright (c) 2022 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\KarriereSuedwestfalenSdk;

use PSVneo\KarriereSuedwestfalenSdk\Struct\DTO\Category;
use PSVneo\KarriereSuedwestfalenSdk\Struct\DTO\Job;
use PSVneo\KarriereSuedwestfalenSdk\Struct\DTO\PaginatedJobSearchResult;
use PSVneo\KarriereSuedwestfalenSdk\Struct\Params\AreaSearchParams;
use PSVneo\KarriereSuedwestfalenSdk\Struct\Params\PaginationParams;

interface ClientInterface
{
    /**
     * @param string|null $type use one of \PSVneo\KarriereSuedwestfalenSdk\Enum\JobType constants.
     * @param string|null $categoryId @todo Currently the API does not respect this option.
     * @param PaginationParams|null $paginationParams If null, api won't respond with paginated result.
     * @param AreaSearchParams|null $areaSearchParams If null, api won't do an area search.
     * @param bool $getRawResult Return the answer of the API as raw assoc array.
     * @return PaginatedJobSearchResult|Job[]|array
     */
    public function searchJobs(
        ?string $type = null,
        ?string $categoryId = null,
        ?PaginationParams $paginationParams = null,
        ?AreaSearchParams $areaSearchParams = null,
        bool $getRawResult = false
    );

    /**
     * @param bool $getRawResult Return the answer of the API as raw assoc array.
     *
     * @return Category[]|array
     */
    public function searchCategories(bool $getRawResult = false): array;
}
