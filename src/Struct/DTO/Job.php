<?php

/**
 * MIT License
 *
 * Copyright (c) 2022 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\KarriereSuedwestfalenSdk\Struct\DTO;

class Job
{
    private string $id;

    private string $type;

    private string $title;

    private string $body;

    private string $fromType;

    private string $fromDate;

    private string $duration;

    private string $durationDate;

    private string $contract;

    private string $leadPosition;

    /**
     * @var Location[]
     */
    private array $locations;

    /**
     * @var ContactPerson[]
     */
    private array $contactPerson;

    private string $extUrl;

    private string $lastRefresh;

    private bool $active;

    private string $imageUrl;

    private string $logo;

    private string $company;

    public function __construct(
        string $id,
        string $type,
        string $title,
        string $body,
        string $fromType,
        string $fromDate,
        string $duration,
        string $durationDate,
        string $contract,
        string $leadPosition,
        array $locations,
        array $contactPerson,
        string $extUrl,
        string $lastRefresh,
        bool $active,
        string $imageUrl,
        string $logo,
        string $company
    ) {
        $this->id = $id;
        $this->type = $type;
        $this->title = $title;
        $this->body = $body;
        $this->fromType = $fromType;
        $this->fromDate = $fromDate;
        $this->duration = $duration;
        $this->durationDate = $durationDate;
        $this->contract = $contract;
        $this->leadPosition = $leadPosition;
        $this->locations = $locations;
        $this->contactPerson = $contactPerson;
        $this->extUrl = $extUrl;
        $this->lastRefresh = $lastRefresh;
        $this->active = $active;
        $this->imageUrl = $imageUrl;
        $this->logo = $logo;
        $this->company = $company;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            (string) ($data['id'] ?? ''),
            (string) ($data['type'] ?? ''),
            (string) ($data['title'] ?? ''),
            (string) ($data['body'] ?? ''),
            (string) ($data['fromType'] ?? ''),
            (string) ($data['fromDate'] ?? ''),
            (string) ($data['duration'] ?? ''),
            (string) ($data['durationDate'] ?? ''),
            (string) ($data['contract'] ?? ''),
            (string) ($data['leadPosition'] ?? ''),
            (array) ($data['locations'] ?? []),
            (array) ($data['contactPerson'] ?? []),
            (string) ($data['extUrl'] ?? ''),
            (string) ($data['lastRefresh'] ?? ''),
            (bool) ($data['active'] ?? false),
            (string) ($data['imageUrl'] ?? ''),
            (string) ($data['logo'] ?? ''),
            (string) ($data['company'] ?? ''),
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    public function getFromType(): string
    {
        return $this->fromType;
    }

    public function setFromType(string $fromType): void
    {
        $this->fromType = $fromType;
    }

    public function getFromDate(): string
    {
        return $this->fromDate;
    }

    public function setFromDate(string $fromDate): void
    {
        $this->fromDate = $fromDate;
    }

    public function getDuration(): string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): void
    {
        $this->duration = $duration;
    }

    public function getDurationDate(): string
    {
        return $this->durationDate;
    }

    public function setDurationDate(string $durationDate): void
    {
        $this->durationDate = $durationDate;
    }

    public function getContract(): string
    {
        return $this->contract;
    }

    public function setContract(string $contract): void
    {
        $this->contract = $contract;
    }

    public function getLeadPosition(): string
    {
        return $this->leadPosition;
    }

    public function setLeadPosition(string $leadPosition): void
    {
        $this->leadPosition = $leadPosition;
    }

    /**
     * @return Location[]
     */
    public function getLocations(): array
    {
        return $this->locations;
    }

    /**
     * @param Location[] $locations
     */
    public function setLocations(array $locations): void
    {
        $this->locations = $locations;
    }

    /**
     * @return ContactPerson[]
     */
    public function getContactPerson(): array
    {
        return $this->contactPerson;
    }

    /**
     * @param ContactPerson[] $contactPerson
     */
    public function setContactPerson(array $contactPerson): void
    {
        $this->contactPerson = $contactPerson;
    }

    public function getExtUrl(): string
    {
        return $this->extUrl;
    }

    public function setExtUrl(string $extUrl): void
    {
        $this->extUrl = $extUrl;
    }

    public function getLastRefresh(): string
    {
        return $this->lastRefresh;
    }

    public function setLastRefresh(string $lastRefresh): void
    {
        $this->lastRefresh = $lastRefresh;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    public function getLogo(): string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): void
    {
        $this->logo = $logo;
    }

    public function getCompany(): string
    {
        return $this->company;
    }

    public function setCompany(string $company): void
    {
        $this->company = $company;
    }
}
