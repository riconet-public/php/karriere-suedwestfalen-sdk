<?php

/**
 * MIT License
 *
 * Copyright (c) 2022 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\KarriereSuedwestfalenSdk\Struct\DTO;

use ArrayIterator;
use IteratorAggregate;

class PaginatedJobSearchResult implements IteratorAggregate
{
    private int $page;

    private int $pages;

    private int $itemsPerPage;

    private int $totalCount;

    /**
     * @var Job[]
     */
    private array $offers;

    public function __construct(int $page, int $pages, int $itemsPerPage, int $totalCount, array $offers)
    {
        $this->page = $page;
        $this->pages = $pages;
        $this->itemsPerPage = $itemsPerPage;
        $this->totalCount = $totalCount;
        $this->offers = $offers;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            (int) ($data['page'] ?? 0),
            (int) ($data['pages'] ?? 0),
            (int) ($data['itemsPerPage'] ?? 0),
            (int) ($data['totalCount'] ?? 0),
            (array) ($data['offers'] ?? []),
        );
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    public function getPages(): int
    {
        return $this->pages;
    }

    public function setPages(int $pages): void
    {
        $this->pages = $pages;
    }

    public function getItemsPerPage(): int
    {
        return $this->itemsPerPage;
    }

    public function setItemsPerPage(int $itemsPerPage): void
    {
        $this->itemsPerPage = $itemsPerPage;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function setTotalCount(int $totalCount): void
    {
        $this->totalCount = $totalCount;
    }

    /**
     * @return Job[]
     */
    public function getOffers(): array
    {
        return $this->offers;
    }

    /**
     * @param Job[] $offers
     */
    public function setOffers(array $offers): void
    {
        $this->offers = $offers;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->getOffers());
    }
}
