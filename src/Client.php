<?php

/**
 * MIT License
 *
 * Copyright (c) 2022 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\KarriereSuedwestfalenSdk;

use Exception;
use GuzzleHttp\Client as HttpClient;
use PSVneo\KarriereSuedwestfalenSdk\Converter\ArrayConverterInterface;
use PSVneo\KarriereSuedwestfalenSdk\Converter\ArrayToCategoriesConverter;
use PSVneo\KarriereSuedwestfalenSdk\Converter\ArrayToJobsConverter;
use PSVneo\KarriereSuedwestfalenSdk\Converter\ArrayToPaginatedJobSearchResultConverter;
use PSVneo\KarriereSuedwestfalenSdk\Converter\SearchJobsArgumentsToQueryConverter;
use PSVneo\KarriereSuedwestfalenSdk\Struct\Params\AreaSearchParams;
use PSVneo\KarriereSuedwestfalenSdk\Struct\Params\PaginationParams;

class Client implements ClientInterface
{
    private const BASE_URL = 'https://www.karriere-suedwestfalen.de/api/v1/';

    private const JOBS_URL = 'companies/%s/offers';

    private const CATEGORIES_URL = 'joboffer-categories';

    private string $kswAuthToken;

    private string $companyId;

    private ArrayConverterInterface $searchJobsArgumentsToQueryConverter;

    private ArrayConverterInterface $arrayToJobsConverter;

    private ArrayConverterInterface $arrayToCategoriesConverter;

    private ArrayConverterInterface $arrayToPaginatedJobSearchResultConverter;

    private array $additionalClientOptions = [];

    public function __construct(string $kswAuthToken, string $companyId, array $additionalClientOptions = [])
    {
        $this->kswAuthToken = $kswAuthToken;
        $this->companyId = $companyId;
        $this->additionalClientOptions = $additionalClientOptions;

        $this->searchJobsArgumentsToQueryConverter = new SearchJobsArgumentsToQueryConverter();
        $this->arrayToJobsConverter = new ArrayToJobsConverter();
        $this->arrayToCategoriesConverter = new ArrayToCategoriesConverter();
        $this->arrayToPaginatedJobSearchResultConverter = new ArrayToPaginatedJobSearchResultConverter();
    }

    public function searchJobs(
        ?string $type = null,
        ?string $categoryId = null,
        ?PaginationParams $paginationParams = null,
        ?AreaSearchParams $areaSearchParams = null,
        bool $getRawResult = false
    ) {
        $client = new HttpClient(array_merge_recursive($this->additionalClientOptions, [
            'base_uri' =>  self::BASE_URL,
            'headers' => [
                'Accept' => 'application/json',
                'KSW-AUTH-TOKEN' => $this->kswAuthToken
            ],
        ]));

        $query = $this->searchJobsArgumentsToQueryConverter->convert([
            'type' => $type,
            'categoryId' => $categoryId,
            'paginationParams' => $paginationParams,
            'areaSearchParams' => $areaSearchParams,
        ]);

        $response = $client->request('GET', sprintf(self::JOBS_URL, $this->companyId), ['query' => $query]);
        $result = json_decode((string) $response->getBody(), true);

        if (200 !== $response->getStatusCode()) {
            throw new Exception('An API error occurred: ' . $response->getBody());
        }

        if ($getRawResult) {
            // Return raw result (assoc array).
            return $result;
        }

        if (is_null($paginationParams)) {
            // Return Job[] (job struct array).
            return $this->arrayToJobsConverter->convert($result);
        }

        // Return PaginatedJobSearchResult object.
        return $this->arrayToPaginatedJobSearchResultConverter->convert($result);
    }

    public function searchCategories(bool $getRawResult = false): array
    {
        $client = new HttpClient(array_merge_recursive($this->additionalClientOptions, [
            'base_uri' =>  self::BASE_URL,
            'headers' => ['Accept' => 'application/json'],
        ]));

        $response = $client->request('GET', self::CATEGORIES_URL);
        $result = json_decode((string) $response->getBody(), true);

        if (200 !== $response->getStatusCode()) {
            throw new Exception('An API error occurred: ' . $response->getBody());
        }

        if ($getRawResult) {
            // Return raw result (assoc array).
            return $result;
        }

        // Return Category[] (Category struct array).
        return $this->arrayToCategoriesConverter->convert($result);
    }

    public function setSearchJobsArgumentsToQueryConverter(
        ArrayConverterInterface $searchJobsArgumentsToQueryConverter
    ): void {
        $this->searchJobsArgumentsToQueryConverter = $searchJobsArgumentsToQueryConverter;
    }

    public function setArrayToJobsConverter(ArrayConverterInterface $arrayToJobsConverter): void
    {
        $this->arrayToJobsConverter = $arrayToJobsConverter;
    }

    public function setArrayToCategoriesConverter(ArrayConverterInterface $arrayToCategoriesConverter): void
    {
        $this->arrayToCategoriesConverter = $arrayToCategoriesConverter;
    }

    public function setArrayToPaginatedJobSearchResultConverter(
        ArrayConverterInterface $arrayToPaginatedJobSearchResultConverter
    ): void {
        $this->arrayToPaginatedJobSearchResultConverter = $arrayToPaginatedJobSearchResultConverter;
    }
}
