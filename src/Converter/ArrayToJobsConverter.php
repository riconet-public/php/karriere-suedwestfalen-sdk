<?php

/**
 * MIT License
 *
 * Copyright (c) 2022 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\KarriereSuedwestfalenSdk\Converter;

use PSVneo\KarriereSuedwestfalenSdk\Struct\DTO\Job;

class ArrayToJobsConverter implements ArrayConverterInterface
{
    /**
     * @return Job[]
     */
    public function convert(array $data): array
    {
        $result = [];
        foreach ($data as $record) {
            if (is_array($record['locations']) && count($record['locations']) > 0) {
                $record['locations'] = (new ArrayToLocationsConverter())->convert($record['locations']);
            }

            if (is_array($record['contactPerson']) && count($record['contactPerson']) > 0) {
                $record['contactPerson'] = (new ArrayToContactPersonConverter())->convert($record['contactPerson']);
            }

            $result[] = Job::fromArray($record);
        }

        return $result;
    }
}
