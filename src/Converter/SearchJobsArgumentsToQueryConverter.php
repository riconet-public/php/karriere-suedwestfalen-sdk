<?php

/**
 * MIT License
 *
 * Copyright (c) 2022 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\KarriereSuedwestfalenSdk\Converter;

use PSVneo\KarriereSuedwestfalenSdk\Enum\JobType;
use PSVneo\KarriereSuedwestfalenSdk\Struct\Params\AreaSearchParams;
use PSVneo\KarriereSuedwestfalenSdk\Struct\Params\PaginationParams;

class SearchJobsArgumentsToQueryConverter implements ArrayConverterInterface
{
    public function convert(array $data): array
    {
        $result = [];

        if (isset($data['type']) && is_string($data['type']) && JobType::isPossibleValue($data['type'])) {
            $result['type'] = $data['type'];
        }

        if (isset($data['categoryId']) && is_string($data['categoryId'])) {
            $result['category'] = $data['categoryId'];
        }

        $result['pagination'] = 'false';
        if (isset($data['paginationParams']) && $data['paginationParams'] instanceof PaginationParams) {
            $result['pagination'] = 'true';
            $result['itemsPerPage'] = $data['paginationParams']->getItemsPerPage();
            $result['_page'] = $data['paginationParams']->getPage();
        }

        if (isset($data['areaSearchParams']) && $data['areaSearchParams'] instanceof AreaSearchParams) {
            $result['lat'] = $data['areaSearchParams']->getLat();
            $result['lon'] = $data['areaSearchParams']->getLon();
            $result['perimeter'] = $data['areaSearchParams']->getPerimeter();
        }

        return $result;
    }
}
