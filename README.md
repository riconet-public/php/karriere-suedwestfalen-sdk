# Karriere Südwestfalen - PHP SDK
This library provides an easy API to communicate with the Karriere Suedwestfalen API.

## Requirements
* PHP 7.4+
* composer
* ext-json

## How to install
Run `composer require psvneo/karriere-suedwestfalen-sdk`.

## How to use

### Instantiate a new client
```php
<?php

require_once '.../vendor/autoload.php';

$token = 'my-secret-token';
$companyId = '12345';

$client = new \PSVneo\KarriereSuedwestfalenSdk\Client($token, $companyId);
```

### Search for jobs

> Yet not documented!

### Search for jobs, using an area search

> Yet not documented!


### Search for categories

> Yet not documented!
